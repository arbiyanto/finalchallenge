# Final Challenge

## Getting Started (Use Terminal)
- git clone https://gitlab.com/arbiyanto/finalchallenge.git
- flutter packages get
- flutter run

## Plugins Error

#### 1. google_api_availability error
if google_api_availability error. Just remove this code from google_api_availability package in the local cache:
```dart
final dynamic availability = await _methodChannel.invokeMethod<String>
```

to

```dart
final dynamic availability = await _methodChannel.invokeMethod(
```

#### 2. Android build error
Geolocator 2.1.1 plugin has an issue with AndroidX support library. The team already made support for AndroidX but the test still failed. It will be fixed in version 2.1.2. We just need to wait. if it is in a hurry, we must release all support for AndroidX

https://github.com/BaseflowIT/flutter-geolocator
