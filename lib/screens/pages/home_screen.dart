import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import './order_map_screen.dart';
import './update_menu_screen.dart';

import '../../helpers/auth.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
    Map _user;
    bool dagang = false;
    Position _position;
    StreamSubscription<Position> positionStream;
    String userId;
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

    @override
    void initState() {
        super.initState();
        Auth.getLocalUser().then((data) {
            setState(() {
                this._user = data;    
            });
        });
        

        _firebaseMessaging.configure(
            onMessage: (Map<String, dynamic> message) async {
                print("onLaunch: $message");
            },
            onLaunch: (Map<String, dynamic> message) async {
                print("onLaunch: $message");
            },
            onResume: (Map<String, dynamic> message) async {
                if(message['type'] == 'pesanan') {
                    Navigator.of(context, rootNavigator: true).push(CupertinoPageRoute(
                        builder: (context) => OrderMapScreen(),
                    ),);
                }
            }
        );
        _firebaseMessaging.requestNotificationPermissions(
            const IosNotificationSettings(sound: true, badge: true, alert: true)
        );
        _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
            print("Settings registered: $settings");
        });

        _firebaseMessaging.getToken().then((token) {
            Auth.getLocalUser().then((data) {
                Firestore.instance.collection('peddlers').document(data['id']).updateData({
                    'notificationToken': token
                });
                print('token sent');
            });
        });
        
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Builder(
                builder: (BuildContext context) {
                    return SafeArea(
                        child: SingleChildScrollView(
                            child: Column(
                                children: <Widget>[
                                    TitleBar(),
                                    Text((this._user != null) ? 'Halo, ' + this._user['name'] : ''),
                                    GestureDetector(
                                        child: BoxSection(section: 'orders'),
                                        onTap: () {
                                            if (this.dagang) {
                                                Navigator.of(context, rootNavigator: true).push(CupertinoPageRoute(
                                                    builder: (context) => OrderMapScreen(),
                                                ),);
                                            }
                                        },
                                    ),
                                    // BoxSection(section: 'favourites'),
                                    GestureDetector(
                                        child: BoxSection(section: 'menu'),
                                        onTap: () {
                                            if (this.dagang) {
                                                Navigator.of(context, rootNavigator: true).push(CupertinoPageRoute(
                                                    builder: (context) => UpdateMenuScreen(),
                                                ),);
                                            }
                                        },
                                    ),
                                    // settingSection(),
                                    ActiveBar(),
                                ],
                            ),
                        )
                    );
                },
            ),
        );
    }

    Widget TitleBar() {
        return Container(
            color: Color.fromRGBO(248, 248, 248, 0.97),
            padding: EdgeInsets.only(bottom: 15,top: 35,right: 15,left: 15),
            child: Row(
                children: <Widget>[
                    Expanded(
                        child: Text('Beranda',
                            style: TextStyle(
                                fontSize: 34,
                                fontWeight: FontWeight.w700,
                            ),
                        )
                    ),
                    Icon(
                        CupertinoIcons.profile_circled,
                        size: 31,
                    ),
                ],
            ),
        );
    }

    Widget ActiveBar() {
        // return Padding(
        //     padding: EdgeInsets.all(15),
        //     child: Row(
        //         children: <Widget>[
        //             Expanded(
        //                 child: (this._user == null) ? SpinKitThreeBounce(
        //                     color: Theme.of(context).primaryColor,
        //                     size: 30,
        //                 ) : Text(this._user['name'],
        //                     style: TextStyle(
        //                         fontSize: 24,
        //                         fontWeight: FontWeight.w500,
        //                     ),
        //                 ),
        //             ),
        //             CupertinoSwitch(
        //                 value: _active,
        //                 onChanged: (bool value) { setState(() { this._active = value; }); },
        //             ),
                    
        //         ],
        //     ),
        // );
        return Container(
            padding: EdgeInsets.only(left: 15,right: 15,top: 50,bottom: 15),
            child: SizedBox(
                width: double.infinity,
                child: CupertinoButton(
                    child: Text(
                        (this.dagang) ? 'Berhenti Berdagang' : 'Mulai Berdagang',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 22,
                        ),
                    ),
                    color: (this.dagang) ? Color.fromRGBO(208, 2, 27, 1.0) : Color.fromRGBO(75, 215, 99, 1),
                    onPressed: () => this.setDagang(),
                ),
            ),
        );
    }

    setDagang() async {
        setState(() {
            this.dagang = (this.dagang) ? false : true;
        });

        Position position;
        final Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
        position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);

        // subscribe location
        // if user phone moved 10 meter from latest position, push to firestore
        try {
            if (this.dagang) {
                var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
                this.positionStream = geolocator.getPositionStream(locationOptions).listen((Position position) {
                    if (position != null) {
                        this._sendPosition(this._user, position);
                    }
                });
            } else {
                this.positionStream.cancel();
            }
        }on PlatformException {

        }
    }

    void _sendPosition(user, position) {
        if (user['id'] != null) {
            Firestore.instance.collection('peddlers').document(user['id']).updateData({
                'location': GeoPoint(position.latitude, position.longitude)
            }).catchError((error) {
                print(error);
            }).then((data) {
                print('berhasil mengirim lokasi');
            });
        }
    }

    Widget BoxSection({
        @required section
    }) {
        LinearGradient gradient;
        dynamic icon;
        String title;
        Widget subtitle;

        TextStyle titleStyle = TextStyle(
            fontSize: 28,
            color: Colors.white,
            fontWeight: FontWeight.w600
        );

        TextStyle subtitleStyle = TextStyle(
            fontSize: 21,
            color: Colors.white,
            fontWeight: FontWeight.w400
        );

        Widget BoxIcon({
            @required AssetImage image,
        }) {
            return Image(
                image: image,
                width: 69,
            );
        }

        switch (section) {
            case 'orders':
                gradient = LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.39, 1.0],
                    colors: [
                        Color.fromRGBO(0, 122, 255, 0.38999998569488525),
                        Color.fromRGBO(0, 122, 255, 1.0)
                    ]
                );

                title = 'Pesanan';
                subtitle = subtitleBuilder(
                    future: Firestore.instance.document('peddlers/$userId').collection('orders').getDocuments(),
                    isEmpty: Text('Belum ada pesanan',style: subtitleStyle),
                    isSuccess: (docLength) {
                        return Text('Ada ' + docLength.toString() + ' Pesanan', style: subtitleStyle);
                    }
                );
                icon = BoxIcon(image: AssetImage('assets/Star.png'));
            break;
            case 'favourites':
                gradient = LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.5, 1.0],
                    colors: [
                        Color.fromRGBO(255, 199, 0, 1.0),
                        Color.fromRGBO(255, 149, 0, 1.0)
                    ]
                );
            
                icon = BoxIcon(image: AssetImage('assets/Contact.png'));

                title = 'Pelanggan Favorit';
                subtitle = subtitleBuilder(
                    future: Firestore.instance.document('peddlers/$userId').collection('favourites').getDocuments(),
                    isEmpty: Text('Belum ada favorit',style: subtitleStyle),
                    isSuccess: (docLength) {
                        return Text('Ada ' + docLength.toString() + ' Favorit', style: subtitleStyle);
                    }
                );
            break;
            case 'menu':
                gradient = LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.5, 1.0],
                    colors: [
                        // Color.fromRGBO(0, 122, 255, 1.0),
                        // Color.fromRGBO(0, 122, 255, 0.38999998569488525),
                        Color.fromRGBO(255, 199, 0, 1.0),
                        Color.fromRGBO(255, 149, 0, 1.0)
                    ]
                );
            
                icon = Icon(Icons.restaurant_menu, size: 69,color: Colors.white,);

                title = 'Tambah Menu';
                subtitle = subtitleBuilder(
                    future: Firestore.instance.document('peddlers/$userId').collection('menu').getDocuments(),
                    isEmpty: Text('Belum ada menu',style: subtitleStyle),
                    isSuccess: (docLength) {
                        return Text('Ada ' + docLength.toString() + ' Menu', style: subtitleStyle,);
                    }
                );
            break;
            default:
        }

        if (!this.dagang) {
            gradient = LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.39, 1.0],
                colors: [
                    Color.fromRGBO(0,0, 0, 0.06),
                    Color.fromRGBO(0,0, 0, 0.1)
                ]
            );
        }

        return Padding(
            padding: EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
            child: Container(
                height: 136,
                width: 338,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                        Radius.circular(23)
                    ),
                    gradient: gradient
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 25, right: 25),
                            child: icon,
                        ),
                        Flexible(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                    Text(title, style: titleStyle,),
                                    subtitle
                                ],
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    subtitleBuilder({
        @required Future<QuerySnapshot> future,
        @required Widget isEmpty,
        @required Widget isSuccess(int documentLength)
    }) {
        return FutureBuilder(
            future: Auth.getLocalUser(),
            builder: (context, snapshot) {
                if (!snapshot.hasData) {
                    return Container();
                }

                this.userId = snapshot.data['id'];

                return FutureBuilder(
                    future: future,
                    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (!snapshot.hasData) {
                            return Container();
                        }

                        var len = snapshot.data.documents.length;

                        if (len < 1) {
                            return isEmpty;
                        }

                        return isSuccess(len);
                    },
                );
            },
        );
    }

    Widget settingSection() {
        Color buttonColor = Color.fromRGBO(255, 149, 0, 1.0);

        return Container(
            padding: EdgeInsets.only(top: 10, bottom: 20, left: 35, right: 35),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                    Column(
                        children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(bottom: 15,left: 15),
                                child: Text('Settings',
                                    style: TextStyle(
                                        fontSize: 21,
                                        fontWeight: FontWeight.w600
                                    ),
                                ),
                            ),
                            Image(
                                image: AssetImage('assets/GerobakLogo.png'),
                                width: 92,
                            )
                        ],
                    ),
                    OutlineButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        borderSide: BorderSide(color: buttonColor),
                        textColor: buttonColor,
                        onPressed: () {
                        },
                        child: Text('+ Tambah Menu',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 14
                            ),
                        ),
                    )
                ],
            ),
        );
    }
}