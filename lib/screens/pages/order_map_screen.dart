import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:scrollable_bottom_sheet/scrollable_bottom_sheet.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import './customer_profile_screen.dart';

import '../../helpers/auth.dart';
import '../../models/order.dart';


class OrderMapScreen extends StatefulWidget {
    @override
    _OrderMapScreenState createState() => _OrderMapScreenState();
}

class _OrderMapScreenState extends State<OrderMapScreen> with WidgetsBindingObserver, TickerProviderStateMixin{ 
    Position _position;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    final key = new GlobalKey<ScrollableBottomSheetState>();
    GoogleMapController mapController;     
    Map _user;
    List customers = [];
    StreamController customersStream = StreamController.broadcast();

    @override
    void initState() {
        super.initState();
        // call after first rendering
        WidgetsBinding.instance.addPostFrameCallback((_) => afterFirstLayout(context));
        // get user data from the first
        Auth.getLocalUser().then((data) {
            this._user = data;
            this.getCustomers();
        });
        // platform init, check user location permission
        _initPlatformState();
    }

    void afterFirstLayout(BuildContext context) {
        this._settingModalBottomSheet(context);
    }

    // Platform messages are asynchronous, so we initialize in an async method.
    Future<void> _initPlatformState() async {
        Position position;

        // Platform messages may fail, so we use a try/catch PlatformException.
        try {
            final Geolocator geolocator = Geolocator()
            ..forceAndroidLocationManager = true;
            position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
            // subscribe location
            // if user phone moved 10 meter from latest position, push to firestore
            // var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
            // StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen((Position position) {
            //     if (position != null) {
                    
            //     }
            //     print(_position == null ? 'Unknown' : _position.latitude.toString() + ', ' + _position.longitude.toString());
            // });
        } on PlatformException {
            position = null;
        }

        // If the widget was removed from the tree while the asynchronous platform
        // message was in flight, we want to discard the reply rather than calling
        // setState to update our non-existent appearance.
        if (!mounted) {
            return;
        }

        setState(() {
            _position = position;
        });
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Builder(
                builder: (BuildContext context) {
                    return Stack(
                        children: <Widget>[
                            Container(
                                child: FutureBuilder(
                                    future: Geolocator().checkGeolocationPermissionStatus(),
                                    builder: (BuildContext context, AsyncSnapshot<GeolocationStatus> snapshot) {
                                        if (!snapshot.hasData) {
                                            return Container(child: Text('Loading'),);
                                        }
                                        if (snapshot.data == GeolocationStatus.disabled) {
                                            return Container(child: Text('Location services disabled, Enable location services for this App using the device settings.'),);
                                        }

                                        if (snapshot.data == GeolocationStatus.denied) {
                                            return Container(child: Text('Access to location denied, Allow access to the location services for this App using the device settings.'),);
                                        }

                                        if (_position != null) {
                                            return Container(
                                                child: initMap()
                                            );
                                        } else {
                                            return Container(
                                                child: CircularProgressIndicator(),
                                            );
                                        }
                                    },
                                ),
                            ),
                            CupertinoNavigationBar(
                                previousPageTitle: 'Beranda',
                                backgroundColor: Color.fromRGBO(255, 255, 255, 0.4),
                            ),
                        ],
                    );
                },
            ),
            key: _scaffoldKey,
        );
    }

    Widget initMap() {
        return GoogleMap(
            initialCameraPosition: CameraPosition(
                target: LatLng(_position.latitude, _position.longitude),
                zoom: 16.0
            ),
            onMapCreated: this._onMapCreated,
            myLocationEnabled: true,
            minMaxZoomPreference: MinMaxZoomPreference(15.0, null),
        );
    }


    Future<Marker> _addMarker({ double latitude, double longitude, String name }){
        if (this.mapController != null) {
            return this.mapController.addMarker(MarkerOptions(
                position: LatLng(latitude, longitude),
                infoWindowText: InfoWindowText(name, ''),
                icon: BitmapDescriptor.fromAsset('assets/MarkerPembeli.png'),
                consumeTapEvents: true
            ));
        }
    }

    Future<void> _updateMarker({ double latitude, double longitude, Marker marker }) {
        return this.mapController.updateMarker(marker, MarkerOptions(
            position: LatLng(latitude, longitude)
        ));
    }

    void _sendPosition(user, position) {
        if (user['id'] != null) {
            Firestore.instance.collection('peddlers').document(user['id']).updateData({
                'location': GeoPoint(position.latitude, position.longitude)
            }).catchError((error) {
                print(error);
            }).then((data) {
                print('berhasil mengirim lokasi');
            });
        }
        
    }

    void _onMapCreated(GoogleMapController controller) {
        setState(() { 
            this.mapController = controller;
            this.getCustomers();
            this._sendPosition(this._user, this._position);
            key.currentState.animateToHalf(context);
        });
    }

    void getCustomers(){
        var snapshots = this.getOrderSnapshots();

        snapshots.forEach((snapshot) {
            if (snapshot.documents.isEmpty) {
                if (!this.customersStream.isClosed) {
                    this.customersStream.add('empty');
                }
                
            }else {
                snapshot.documents.forEach((record) {
                    Stream<DocumentSnapshot> customerSnapshot = Firestore.instance.collection('users').document(record.data['userID']).snapshots();
                    customerSnapshot.listen((customer) {
                        // customer data from new firestore
                        Map<String, dynamic> customerData = customer.data;
                        
                        // check if customer data in local
                        var savedCustomer = this.customers.firstWhere((c) => c['id'] == customerData['id'], orElse: () => null);
                        var savedIndex = this.customers.indexOf(savedCustomer);
                        
                        if (savedCustomer != null) {
                            customerData['marker'] = savedCustomer['marker'];
                            this.customers[savedIndex] = customerData;
                            this.customerUpdate(customerData);
                        } else {
                            this.customerInsert(customerData);
                        }
                        
                    });
                });
            }
            
        });
    }

    // will return stream snapshot or future snapshot
    Stream<QuerySnapshot> getOrderSnapshots() {
        return Firestore.instance.collection('peddlers').document(this._user['id']).collection('orders').snapshots();
    }

    void customerInsert(customerData) async{
        GeoPoint location = customerData['location'];
        String name = customerData['nickname'];

        // if customer not in local, create new marker
        var marker = await this._addMarker(
            longitude: location.longitude, 
            latitude: location.latitude, 
            name: name
        );

        if (marker != null) {
            // save customer marker
            customerData['marker'] = marker;
            this.customers.add(customerData);

            if (!this.customersStream.isClosed) {
                this.customersStream.add('data');
            }
            
        }
    }

    void customerUpdate(customerData) {
        GeoPoint location = customerData['location'];

        // if customer in local, update marker
        this._updateMarker(
            longitude: location.longitude, 
            latitude: location.latitude, 
            marker: customerData['marker']
        );

        if (!this.customersStream.isClosed) {
            this.customersStream.add('data');
        }
    }

    

    
    void _settingModalBottomSheet(context) async{
        await _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
            return _bottomSheetBuilder(context);
        });
    }

    bool firstInit = false;
    
    Widget _bottomSheetBuilder(BuildContext context) {
        return Stack(children: [
            ScrollableBottomSheet(
                key: key,
                halfHeight: MediaQuery.of(context).size.height * 35 / 100,
                minimumHeight: MediaQuery.of(context).size.height * 15 / 100,
                autoPop: false,
                scrollTo: ScrollState.minimum,
                snapAbove: true,
                snapBelow: true,
                callback: (state) {
                    // if (state == ScrollState.minimum) {
                    //     if (!firstInit) {
                    //         this.firstInit = true;
                    //         Future.delayed(Duration(milliseconds: 45), () {
                                
                    //         });
                    //     }
                    // }
                },
                child: myBottomSheet()
            ),
        ]);
    }

    BoxDecoration sheetDecoration = BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(30.0),
            topRight: const Radius.circular(30.0)
        ),
        boxShadow: <BoxShadow>[
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 1.0),
                offset: Offset(0, 5.0),
                blurRadius: 4.0
            ),
        ]
    );

    Widget myBottomSheet() {
        return Container(  
            decoration: sheetDecoration,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: myColumn(),
        );
    }
    
    Widget myColumn() {
        return StreamBuilder(
            stream: this.customersStream.stream,
            builder: (context, AsyncSnapshot snapshot) {
                var customersSnap = this.customers;

                if (snapshot.data == 'empty') {
                    return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(top: 80),
                                child: Text(
                                    'Belum ada pesanan',
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromRGBO(33, 33, 33, 0.5)
                                    ),
                                ),
                            )
                        ],
                    );
                }

                if (this.customers.length > 0) {
                    return Column(
                        children: customersSnap.map((data) => listItem(context, data)).toList()
                    );
                } else {
                    return Column(
                        children: [SpinKitThreeBounce(
                            size: 50,
                            color: Theme.of(context).primaryColor,
                        )],
                    );
                }
            },
        );
    } 

    Widget listItem(BuildContext context, data) {
        OrderUser order = OrderUser.fromMap(data);

        return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            key: ValueKey(order.id),
            child: ListTile(
                    leading: CircleAvatar(
                    // backgroundImage: Image.network('https://i.pinimg.com/736x/cb/38/8b/cb388b0bec7f9e332883bd4ab37dee61.jpg').image,
                    backgroundImage: Image.asset('assets/user.png').image,
                ),
                title: Text(
                    order.nickname,
                    style: TextStyle(fontWeight: FontWeight.w500)
                ),
                subtitle: FutureBuilder(
                    future: Geolocator().checkGeolocationPermissionStatus(),
                    builder: (BuildContext context, AsyncSnapshot<GeolocationStatus> snapshot) {
                        if (_position != null) {
                            return Text(
                                this._calculateDistance(
                                    targetLatitude: order.location.latitude,
                                    targetLongitude: order.location.longitude
                                )
                            );
                        } else {
                            return SpinKitThreeBounce(
                                color: Theme.of(context).primaryColor,
                                size: 30,
                            );
                        }
                    }
                ),
                trailing: IconButton(
                    icon: Icon(Icons.favorite_border),
                ),
                onTap: () {
                    Navigator.of(context).push(CupertinoPageRoute(
                        builder: (context) => CustomerProfileScreen(
                            customer: order,
                        ),
                        fullscreenDialog: true
                    ),);
                },
            ),
        );
    }

    _calculateDistance({
        @required double targetLatitude,
        @required double targetLongitude
    }) {
        // satu derajat longitude dan latitude dalam meter
        double lat2mtr = 110574;
        double lon2mtr = 111320;

        double currentLatitude = this._position.latitude;
        double currentLongitude = this._position.longitude;

        // jarak berdasarkan derajat long/lat = long/lat 1 - long/lat 2
        double deltaLatInDegrees = currentLatitude - targetLatitude;
        double deltaLonInDegrees = currentLongitude - targetLongitude;

        // convert derajat ke satuan meter
        double deltaLatInMtr = deltaLatInDegrees * lat2mtr;
        double deltaLonInMtr = deltaLonInDegrees * lon2mtr;
        
        // √(DeltaLatitudeMeter^2 + DeltaLongitudeMeter^2)
        double distMtr = sqrt(pow(deltaLatInMtr, 2) + pow(deltaLonInMtr, 2));

        // calculate estimated time of arrival
        double peddlerSpeed = 1.4;
        double eta = (distMtr/peddlerSpeed) / 60;

        String etaString = eta.toStringAsFixed(1);

        return '$etaString menit dari lokasi kamu';
    }

    @override
    void dispose() {
        super.dispose();
        this.customersStream.close(); //Streams must be closed when not needed
    }
}



// var accessToken = 'pk.eyJ1IjoiYXJiaXlhbnRvIiwiYSI6ImNqcjAzNXIxbDBnd200NHFxNG9za3l0YWMifQ.OmffRPA_vcVgXG4WdYLNlw';
// var urlTemplate = "https://api.mapbox.com/styles/v1/arbiyanto/{id}/tiles/256/{z}/{x}/{y}?&access_token={accessToken}";

 // Widget myMap() {
    //     return FlutterMap(
    //         options: MapOptions(
    //             center: LatLng(_position.latitude, _position.longitude),
    //             zoom: 20.0,
    //         ),
    //         layers: [
    //             TileLayerOptions(
    //                 urlTemplate: this.urlTemplate,
    //                 additionalOptions: {
    //                     'accessToken': this.accessToken,
    //                     'id': 'cjr0bfzla1qka2ro5jyhgat3u',
    //                 },
    //             ),
    //             MarkerLayerOptions(
    //                 markers: [
    //                     Marker(
    //                         width: 80.0,
    //                         height: 80.0,
    //                         point: LatLng(_position.latitude, _position.longitude),
    //                         builder: (ctx) => Container(
    //                             child: FlutterLogo(),
    //                         )
    //                     )
    //                 ]
    //             )
    //         ],
    //     );
    // }