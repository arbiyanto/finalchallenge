import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../models/order.dart';

class CustomerProfileScreen extends StatefulWidget {
    final OrderUser customer;

    CustomerProfileScreen({
        @required this.customer
    });

    @override
    _CustomerProfileScreenState createState() => _CustomerProfileScreenState();
}

class _CustomerProfileScreenState extends State<CustomerProfileScreen> {
    String phone;
    final GlobalKey key = GlobalKey();

    @override
    Widget build(BuildContext context) {
        this.phone = widget.customer.phone;

        return CupertinoPageScaffold(
            key: key,
            navigationBar: CupertinoNavigationBar(),
            child: SafeArea(
                child: Column(
                            children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(top: 25,bottom: 25),
                            child: Center(
                                child: CircleAvatar(
                                    backgroundImage: Image.network('https://i.pinimg.com/736x/cb/38/8b/cb388b0bec7f9e332883bd4ab37dee61.jpg').image,
                                    radius: 50,
                                ),
                            ),
                        ),
                        Center(
                            child: Text(
                                widget.customer.nickname,
                                style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.w600
                                ),
                            ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 25),
                            child: Center(
                                child: CupertinoButton(
                                    child: Text(
                                        'Hubungi Pelanggan',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                        ),
                                    ),
                                    color: Color.fromRGBO(244,181,78, 1.0),
                                    // borderRadius: BorderRadius.all(
                                    //     Radius.zero
                                    // ),
                                    onPressed: () {
                                        this.goToWhatsapp();
                                    },
                                ),
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    goToWhatsapp() async {
        var whatsappUrl ="https://wa.me/$phone";
        print(this.phone);
        if(await canLaunch(whatsappUrl)) {
            launch(whatsappUrl);
        } else {
            showMyDialog();
            print('tidak ada whatsapp terinstall');
        }
    }

    showMyDialog() {
        showDialog(context: context, builder: (context) =>  CupertinoAlertDialog(
            title: Text('Tidak Ada Whatsapp Terinstall'),
            content: Text('Silahkan install terlebih dahulu.'),
            actions: <Widget>[
                CupertinoDialogAction(
                    child: const Text('Kembali'),
                    isDefaultAction: true,
                    onPressed: () {
                        Navigator.pop(context, 'Cancel');
                    },
                ),
            ],
        ));
    }
}