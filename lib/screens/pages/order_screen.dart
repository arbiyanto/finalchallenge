import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../helpers/auth.dart';

import '../../models/order.dart';

class OrderScreen extends StatefulWidget {
    @override
    _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
    Map _user;

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Builder(
                builder: (context) {
                    return SafeArea(
                        child: Column(
                            children: <Widget>[
                                Container(
                                    color: Color.fromRGBO(248, 248, 248, 0.97),
                                    padding: EdgeInsets.only(bottom: 15,top: 35,right: 15,left: 15),
                                    child: Row(
                                        children: <Widget>[
                                            Text('Pesanan',
                                                style: TextStyle(
                                                    fontSize: 34,
                                                    fontWeight: FontWeight.w700,
                                                ),                                        
                                            )
                                        ],
                                    ),
                                ),
                                FutureBuilder(
                                    future: Auth.getLocalUser(),
                                    builder: (BuildContext context, snapshot) {
                                        if (snapshot.hasData) {
                                            this._user = snapshot.data;

                                            return FutureBuilder<QuerySnapshot>(
                                                future: Firestore.instance.collection('peddlers').document(_user['id']).collection('orders').getDocuments(),
                                                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                                    if (!snapshot.hasData) {
                                                        return Center(
                                                            child: SpinKitThreeBounce(
                                                                size: 50,
                                                                color: Theme.of(context).primaryColor,
                                                            ),
                                                        );
                                                    }

                                                    if (snapshot.data.documents.isEmpty) {
                                                        return Center(
                                                            child: Text('Belum Ada Pesanan'),
                                                        );
                                                    }

                                                    return Column(
                                                        children: snapshot.data.documents.map<Widget>((data) => listOrder(data)).toList(),
                                                    );
                                                },
                                            );
                                        }

                                        return Container();
                                    }
                                )
                            ]
                        )
                    );
                },
            ),
        );
    }

    Widget listOrder(data) {
        Order order = Order.fromMap(data);

        return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            key: ValueKey(order.userID),
            child: ListTile(
                    leading: CircleAvatar(
                    backgroundImage: Image.asset('assets/user.png').image,
                ),
                title: Text(
                    order.userName,
                    style: TextStyle(fontWeight: FontWeight.w500)
                ),
                trailing: IconButton(
                    icon: Icon(Icons.favorite_border),
                ),
            ),
        );
        // return FutureBuilder(
        //     future: Firestore.instance.collection('users').document(data['userID']).get(),
        //     builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        //         if (!snapshot.hasData) {
        //             return Container();
        //         }

        //         return Padding(
        //             padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        //             key: ValueKey(order.id),
        //             child: ListTile(
        //                     leading: CircleAvatar(
        //                     backgroundImage: Image.network('https://i.pinimg.com/736x/cb/38/8b/cb388b0bec7f9e332883bd4ab37dee61.jpg').image,
        //                 ),
        //                 title: Text(
        //                     order.nickname,
        //                     style: TextStyle(fontWeight: FontWeight.w500)
        //                 ),
        //                 trailing: IconButton(
        //                     icon: Icon(Icons.favorite_border),
        //                 ),
        //             ),
        //         );
        //     },
        // );

    }

    
}