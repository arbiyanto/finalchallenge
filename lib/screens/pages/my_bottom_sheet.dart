import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class MyBottomSheet extends StatefulWidget {
  MyBottomSheet({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyBottomSheet createState() => _MyBottomSheet();
}

class _MyBottomSheet extends State<MyBottomSheet> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new 
        GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: (){
          _settingModalBottomSheet(context);
        },
        // tooltip: 'Increment',
        child: new Icon(Icons.add),
      ),
      key: _scaffoldKey,
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(icon, color: color,),
          // tooltip: 'a tool tip',
          onPressed: (){print('$label pressed');},
        ),
        Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

  void _settingModalBottomSheet(context){
    // create bottom sheet
    _scaffoldKey.currentState
      .showBottomSheet<Null>((BuildContext context) {

      Color color = Theme.of(context).primaryColor;

      Widget buttonSection = Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildButtonColumn(color, Icons.call, 'Makanan'),
            _buildButtonColumn(color, Icons.near_me, 'Minuman'),
            _buildButtonColumn(color, Icons.share, 'Sayuran'),
            _buildButtonColumn(color, Icons.more, 'Lainnya'),
          ],
        ),
      );

      return new Container(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // new Padding(
            //     padding: const EdgeInsets.all(16.0),
            //     child: new Text(
            //       'Persistent header for bottom bar!',
            //       textAlign: TextAlign.center,
            //     )
            // ),
            buttonSection,
            new ListTile(
              leading: new Icon(Icons.music_note),
              title: new Text('Sate Bawah Pohon',
                    style: TextStyle(fontWeight: FontWeight.w500)),
              subtitle: new Text('100 meter'),
              trailing: IconButton(
                          icon: Icon(Icons.favorite_border),
                          onPressed: (){print('Liked');},
                        ),
              // onTap: () => ...,          
            ),
            new ListTile(
              leading: new Icon(Icons.photo_album),
              title: new Text('Mie Goreng Bang Saleh', 
                    style: TextStyle(fontWeight: FontWeight.w500)),
              subtitle: new Text('200 meter'),
              trailing: IconButton(
                          icon: Icon(Icons.favorite_border),
                          onPressed: (){print('Liked');},
                        ),
              // onTap: () => ...,          
            ),
            new ListTile(
              leading: new Icon(Icons.videocam),
              title: new Text('Ketoprak', 
                    style: TextStyle(fontWeight: FontWeight.w500)),
              subtitle: new Text('203 meter'),
              trailing: IconButton(
                          icon: Icon(Icons.favorite_border),
                          onPressed: (){print('Liked');},
                        ),
              // onTap: () => ...,          
            ),
          ],
        )
      );
    });
  }
}