import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../helpers/auth.dart';
import '../logins/components/peddlers.dart';

class UpdateMenuScreen extends StatefulWidget {
    @override
    _UpdateMenuScreenState createState() => _UpdateMenuScreenState();
}

class _UpdateMenuScreenState extends State<UpdateMenuScreen> {
    List<Map> data = [];
    List<Map> tempData = [];

    List isUploading = [];
    var user;
    bool isLoading = false;
    bool isDisabled = false;
    bool firstBuild = true;

    @override
    Widget build(BuildContext context) {
        if (firstBuild) {
            this.isUploading = List<bool>.generate(30, (i) => false);
            this.firstBuild = false;
        }

        return CupertinoPageScaffold(
            backgroundColor: Color.fromRGBO(245, 245, 245, 1),
            navigationBar: CupertinoNavigationBar(
                previousPageTitle: 'Beranda',
            ),
            child: Stack(children: <Widget>[
                SafeArea(
                    child: SingleChildScrollView(
                        child: Container(
                            padding: EdgeInsets.only(top: 35,bottom: 85,left: 25,right: 25),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                    Padding(
                                        padding: const EdgeInsets.only(bottom: 15),
                                        child: Text('Tambah Menu', style: TextStyle(
                                            fontSize: 28,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromRGBO(255, 149, 0, 1)
                                        ),),
                                    ),
                                    FutureBuilder(
                                        future: Auth.getLocalUser(),
                                        builder: (context, snapshot) {
                                            if (!snapshot.hasData) {
                                                return Container();
                                            }

                                            this.user = snapshot.data;
                                            String user_id = this.user['id'];

                                            return FutureBuilder(
                                                future: Firestore.instance.document('peddlers/$user_id').collection('menu').getDocuments(),
                                                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                                    if (!snapshot.hasData) {
                                                        return Container();
                                                    }

                                                    int i = -1;
                                                    List<DocumentSnapshot> documents = snapshot.data.documents;
                                                    this.data = [];

                                                    if (documents.length > 0) {
                                                        documents.forEach((DocumentSnapshot d) {
                                                            // convert string to number
                                                            double harga = double.parse(d['harga'].replaceAll(RegExp('[^0-9]+'), ''));
                                                            
                                                            this.data.add({
                                                                'menuPic': (d['menuPic'] != null) ? d['menuPic'] : '',
                                                                'harga': MoneyMaskedTextController(
                                                                    decimalSeparator: '', 
                                                                    precision: 0,
                                                                    thousandSeparator: '.',
                                                                    leftSymbol: 'Rp ',
                                                                    initialValue: (d['harga'] != null) ? harga : 0.0
                                                                ),
                                                                'name': TextEditingController(text: (d['name'] != null) ? d['name'] : ''),
                                                                'id': d.documentID
                                                            });
                                                        });

                                                        if (tempData.length > 0) {
                                                            tempData.forEach((d) {
                                                                this.data.add(d);
                                                            });
                                                        }
                                                    } else {
                                                        this.data.add({
                                                            'menuPic' : '', 
                                                            'harga': MoneyMaskedTextController(
                                                                decimalSeparator: '', 
                                                                precision: 0,
                                                                thousandSeparator: '.',
                                                                leftSymbol: 'Rp '
                                                            ), 
                                                            'name': TextEditingController(text: ''),
                                                            'id': null
                                                        });
                                                    }
                                                    
                                                    return Column(
                                                        children: data.map<Widget>((data) => listMenu(data, i+=1)).toList(),
                                                    );
                                                },
                                            );
                                        },
                                    ),
                                    Center(
                                        child: OutlineButton(
                                            child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                    Icon(Icons.add, color: Colors.orange,size: 16,),
                                                    Text('Tambah Menu Lagi', style: TextStyle(
                                                        color: Colors.orange
                                                    ),),
                                                ],
                                            ),
                                            color: Colors.orange,
                                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                                            onPressed: () {
                                                setState(() {
                                                    tempData.add({ 'menuPic' : '', 'harga': MoneyMaskedTextController(precision: 0, thousandSeparator: '.', decimalSeparator: '', leftSymbol: 'Rp '), 'name': TextEditingController(text: ''),  });
                                                });
                                                print(data.length);
                                            },
                                        ),
                                    ),

                                ],
                            ),
                        ),
                    ),
                ),
                Stack(
                    children: <Widget>[
                        Positioned(
                            left: 0,
                            right: 0,
                            bottom: 0,
                            child: Container(
                                height: 60,
                                child: SizedBox.expand(
                                    child: CupertinoButton(
                                        child: (!isLoading) ? Text(
                                            'Simpan',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w700,
                                                fontSize: 22,
                                                letterSpacing: 0.35
                                            ),
                                        ) : SpinKitThreeBounce(
                                            color: Colors.white,
                                            size: 35.0,
                                        ),
                                        color: Color.fromRGBO(255, 149, 0, 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.zero
                                        ),
                                        onPressed: isDisabled ? null : () {
                                            submit();
                                        },
                                    )
                                ),
                            ),
                        ),
                    ],
                )
            ],)
        );
    }

    Widget listMenu(menu, index) {
        return Container(
            padding: EdgeInsets.only(bottom: 15),
            child: Row(
                children: <Widget>[
                    GestureDetector(
                        onTap: () {
                            getImage(menu, index);
                        },
                        child: (menu['menuPic'] != '') ? FutureBuilder(
                            future: FirebaseStorage.instance.ref().child(menu['menuPic']).getDownloadURL(),
                            builder: (context, snapshot) {
                                Widget template({
                                    String image
                                }) { 
                                    return Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5)
                                            ),
                                            color: Color.fromRGBO(216, 216, 216, 0.54),
                                            image: (image != '') ? DecorationImage(
                                                fit: BoxFit.cover,
                                                alignment: FractionalOffset.topCenter,
                                                image: CachedNetworkImageProvider(snapshot.data.toString())
                                            ) : null
                                        ),
                                        height: 100,
                                        width: 100,
                                        child: (!isUploading[index]) ? null : LoadingUpload()
                                    );
                                }
                                
                                if (!snapshot.hasData) {
                                    return template(image: '');
                                }

                                String url = snapshot.data.toString();
                                return template(image: url);
                            }
                        ) : Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5)
                                ),
                                color: Color.fromRGBO(216, 216, 216, 0.54),
                            ),
                            height: 100,
                            width: 100,
                            child: (!isUploading[index]) ? CameraIcon() : LoadingUpload(),
                        ),
                    ),
                    Expanded(
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Column(
                                children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: CupertinoTextField(
                                            controller: menu['name'],
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(255, 255, 255, 0.7),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(2)
                                                ),
                                            ),
                                            style: TextStyle(
                                                color: Color.fromRGBO(33, 33, 3, 1),
                                                fontSize: 16
                                            ),
                                            placeholder: 'Nama Menu',
                                            keyboardType: TextInputType.text,
                                        ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: CupertinoTextField(
                                            controller: menu['harga'],
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(255, 255, 255, 0.7),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(2)
                                                ),
                                            ),
                                            style: TextStyle(
                                                color: Color.fromRGBO(33, 33, 3, 1),
                                                fontSize: 16
                                            ),
                                            placeholder: 'Harga. Contoh: 15000',
                                            keyboardType: TextInputType.number,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    )
                ],
            ),
        );
    }

    Widget CameraIcon() {
        return Icon(
            Icons.photo_camera,
            size: 20,
            color: Color.fromRGBO(170, 41, 23, 1.0),
        );
    }

    Widget LoadingUpload() {
        return Center(
            child: SpinKitCircle(
                size: 20,
                color: Colors.orange,
            )
        );
    }

    Future getImage(menu, index) async {
        var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 270, maxHeight: 270);

        if (tempImage == null ) {
            print('canceled');
        } else {
            final String uuid = Uuid().v1();
            final String filename = uuid+'.png';

            setState(() {
                this.isUploading[index] = true;
                print(this.isUploading[index]);
            });

            // PeddlerService.isNextDisabled.add(true);

            final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(filename);
            final StorageUploadTask task = firebaseStorageRef.putFile(tempImage);

            task.onComplete.then((d) {
                if(task.isSuccessful) {
                    // set to global
                    menu['menuPic'] = filename;
                    // PeddlerService.isNextDisabled.add(false);
                    setState(() {
                        this.isUploading[index] = false;
                    });
                    print('success uploading');
                }

                if (task.isCanceled) {
                    print('upload canceled');
                }
            });
        }
        
    }

    Future<bool> submit() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final user_id = prefs.getString('id');
        final DocumentReference postRef = Firestore.instance.document('peddlers/$user_id');
        setState(() {
            this.isDisabled = true;
            this.isLoading = true;
        });

        // TODO: Handle Error
        data.forEach((d) async{
            if (d['name'].text != null || d['name'].text != '') {
                String generatedId = (d['id'] == null) ? Uuid().v4() : d['id'];
                await Firestore.instance.document('peddlers/$user_id').collection('menu').document(generatedId).setData({
                    'menuPic': d['menuPic'],
                    'harga': (d['harga'] != null) ? d['harga'].text : '',
                    'name': d['name'].text,
                    'description': ''
                });
            }
        });

        setState(() {
            this.isDisabled = false;
            this.isLoading = false;
        });
        
        Navigator.pop(context);
    }
}