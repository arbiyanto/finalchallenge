// import dependencies
import 'package:flutter/cupertino.dart';

// import screens
import './home_screen.dart';
import './order_screen.dart';
import './chat_list_screen.dart';
import './account_screen.dart';

// import helpers
import '../../helpers/custom_icon.dart';


const bottomTabBarItems = <BottomNavigationBarItem> [
    BottomNavigationBarItem(
        icon: const CustomIcon('assets/BerandaNonActive.png'),
        activeIcon: const CustomIcon('assets/BerandaActive.png'),
        title: Text('Beranda'),
    ),
    BottomNavigationBarItem(
        icon: const CustomIcon('assets/OrderNonActive.png'),
        activeIcon: const CustomIcon('assets/OrderActive.png'),
        title: Text('Pesanan'),
    ),
    // BottomNavigationBarItem(
    //     icon: const CustomIcon('assets/ObrolanNonActive.png'),
    //     activeIcon: const CustomIcon('assets/ObrolanActive.png'),
    //     title: Text('Obrolan'),
    // ),
];

class Template extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
            items: bottomTabBarItems,
            activeColor: const Color.fromRGBO(244,181,78, 1.0),
        ),
        tabBuilder: (BuildContext context, int index) {
            return CupertinoTabView(
                builder: (BuildContext context) {
                    assert(index >= 0 && index <= 2);
                    switch (index) {
                        case 0:
                            return CupertinoTabView(
                                builder: (BuildContext context) {
                                    return HomeScreen();
                                },
                                defaultTitle: 'Home',
                            );
                        break;
                        case 1:
                            return CupertinoTabView(
                                builder: (BuildContext context) {
                                    return OrderScreen();
                                },
                                defaultTitle: 'Pesanan',
                            );
                        break;
                        case 2:
                            return CupertinoTabView(
                                builder: (BuildContext context) {
                                    return ChatListScreen();
                                },
                                defaultTitle: 'Chat',
                            );
                        case 3:
                            return CupertinoTabView(
                                builder: (BuildContext context) {
                                    return AccountScreen();
                                },
                                defaultTitle: 'Account',
                            );
                    }
                },
            );
        },
    );
  }
}