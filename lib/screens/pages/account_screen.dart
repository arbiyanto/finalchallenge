import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountScreen extends StatefulWidget {
    @override
    _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
    // final GoogleSignIn _googleSignIn = GoogleSignIn();
    // final FirebaseAuth _auth = FirebaseAuth.instance;
    SharedPreferences prefs;

    bool isLoading = false;
    bool isLoggedIn = false;
    FirebaseUser currentUser;

    @override
    void initState() {
        super.initState();
        isSignedIn();
    }

    // check if logged in or not
    void isSignedIn() async {
        // initial loading
        this.setState(() {
            this.isLoading = true;
        });

        // check local data
        prefs = await SharedPreferences.getInstance();

        // isLoggedIn = await this._googleSignIn.isSignedIn();
        // if (isLoggedIn) {
        //     // redirect to another page
        // }

        // remove loading after checking
        this.setState(() {
            this.isLoading = false;
        });
    }

    @override
    Widget build(BuildContext context) {
        return CupertinoPageScaffold(
            child: Center(
                child: CupertinoButton(
                    child: Text('Sign in with Google'),
                    color: Color.fromRGBO(66, 165, 245, 1.0),
                    onPressed: () {
                        // _handleSignIn()
                        //     .then((FirebaseUser user) => print(user))
                        //     .catchError((e) => print(e));
                    }
                ),
            ),
        );
    }

    // handle sign in with google
    Future<FirebaseUser> _handleSignIn() async {
        // GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        // GoogleSignInAuthentication googleAuth = await googleUser.authentication;
        // FirebaseUser user = await _auth.signInWithGoogle(
        //     accessToken: googleAuth.accessToken,
        //     idToken: googleAuth.idToken,
        // );

        // this.setState(() {
        //     this.isLoading = true;
        // });

        // if (user != null) {
        //     // check if user already sign up
        //     final QuerySnapshot result =
        //     await Firestore.instance.collection('users').where('id', isEqualTo: user.uid).getDocuments();
        //     final List<DocumentSnapshot> documents = result.documents;

        //     if (documents.length == 0) {
        //         // Update data to server if new user
        //         Firestore.instance.collection('users').document(user.uid).setData(
        //         {'nickname': user.displayName, 'photoUrl': user.photoUrl, 'id': user.uid});
        //     } else {
        //         // Write data to local
        //         await prefs.setString('id', documents[0]['id']);
        //         await prefs.setString('nickname', documents[0]['nickname']);
        //         await prefs.setString('photoUrl', documents[0]['photoUrl']);
        //         await prefs.setString('aboutMe', documents[0]['aboutMe']);
        //     }

        //     print('sign in success');

        //     this.setState(() {
        //         isLoading = false;
        //     });
        // } else {
        //     this.setState(() {
        //         isLoading = false;
        //     });

        //     print('Sign in failed');
        // }
        
        // return user;
        
    }
}