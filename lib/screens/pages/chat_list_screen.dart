import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../helpers/auth.dart';
import '../../models/chat.dart';
import '../../models/user.dart';

import './chat_screen.dart';

class ChatListScreen extends StatefulWidget {
    @override
    _ChatListScreenState createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
    Map _user;
    String _lastDocumentId = null;

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Builder(
                builder: (BuildContext context) {
                    return SafeArea(
                        // check local data auth
                        child: Column(
                            children: <Widget>[
                                Container(
                                    color: Color.fromRGBO(248, 248, 248, 0.97),
                                    padding: EdgeInsets.only(bottom: 15,top: 35,right: 15,left: 15),
                                    child: Row(
                                        children: <Widget>[
                                            Text('Obrolan',
                                                style: TextStyle(
                                                    fontSize: 34,
                                                    fontWeight: FontWeight.w700,
                                                ),                                        
                                            )
                                        ],
                                    ),
                                ),
                                // Chat List
                                FutureBuilder(
                                    future: Auth.getLocalUser(),
                                    builder: (BuildContext context, snapshot) {
                                        if (snapshot.hasData) {
                                            this._user = snapshot.data;
                                            // get chat list
                                            return StreamBuilder<QuerySnapshot>(
                                                stream: Chat.getChats(peddler_id: this._user['id'], last_document_id: this._lastDocumentId),
                                                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                                    if (!snapshot.hasData) {
                                                        return Center(
                                                            child: SpinKitThreeBounce(
                                                                color: Theme.of(context).primaryColor,
                                                                size: 50,
                                                            )
                                                        );
                                                    }

                                                    // if (snapshot.data.documents.length > 0) {
                                                    //     var lastData = snapshot.data.documents.last;
                                                    //     this._lastDocumentId = Chat.fromMap(lastData.data, reference: lastData.reference).id;
                                                    // }

                                                    return Flexible(
                                                        child: CustomScrollView(
                                                            shrinkWrap: true,
                                                            slivers: <Widget>[
                                                                SliverList(
                                                                    delegate: SliverChildListDelegate(
                                                                        snapshot.data.documents.map<Widget>((document) => chatItem(context, document)).toList()
                                                                    )
                                                                )
                                                            ]
                                                        )
                                                    );
                                                },
                                            );
                                        }
                                        return Container();
                                    },
                                )
                            ],
                        )
                    );
                },
            ),
        );
    }

    chatItem(BuildContext context, DocumentSnapshot document) {
        Chat data = Chat.fromSnapshot(document);
        
        // TODO change this widget into custom list
        // Wrap it with padding and include the image from storage inside
        return Container(
            key: ValueKey(data.id),
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: FutureBuilder(
                future: this.getUserData(data.userId),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                        return SpinKitThreeBounce(
                            color: Theme.of(context).primaryColor,
                            size: 50,
                        );
                    }

                    return _ChatItem(
                        leading: CircleAvatar(
                            radius: 30.0,
                            child: FlutterLogo(),
                            // Image.network(snapshot.data.photoUrl),
                        ),
                        title: Text(
                            snapshot.data.nickname,
                            style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                            ),
                            overflow: TextOverflow.fade,
                        ),
                        subtitle: Padding(
                            padding: const EdgeInsets.only(top: 3),
                            child: Text(
                                data.lastMessageBody,
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 13,
                                ),
                            ),
                        ),
                        time: Text(
                            data.lastMessageSent.toDate().hour.toString() + ':' + data.lastMessageSent.toDate().minute.toString(),
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400
                            ),
                        ),
                        onTap: () {
                            Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (context) => ChatScreen(
                                        chatId: data.id,
                                        peddler: this._user,
                                        user: snapshot.data,
                                    )
                                )
                            );
                        },
                    );

                },
            ),
        );
    }

    Future getUserData(String userId) async {
        QuerySnapshot snapshot = await Firestore.instance.collection('users').where('id', isEqualTo: userId).getDocuments();
        return User.fromSnapshot(snapshot.documents.first);
    }
}

class _ChatItem extends StatelessWidget {
    final GestureTapCallback onTap;
    final Widget leading;
    final Widget title;
    final Widget subtitle;
    final Widget time;

    const _ChatItem({
        this.onTap,
        this.leading,
        this.title,
        this.subtitle,
        this.time
    });

    @override
    Widget build(BuildContext context) {
        return GestureDetector(
            onTap: onTap,
            child: Container(
                padding: EdgeInsets.only(left: 15,right: 15),
                child: Column(
                    children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: Row(
                                children: <Widget>[
                                    leading,
                                    Expanded(
                                        child: Padding(
                                            padding: EdgeInsets.only(left: 10),
                                            child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                                title,
                                                subtitle
                                            ],
                                        ),
                                        ),
                                    ),
                                    Stack(
                                        overflow: Overflow.visible,
                                        children: <Widget>[
                                            Positioned(
                                                right: 25,
                                                top: 0,
                                                child: time,
                                            ),
                                            Icon(
                                                Icons.chevron_right, 
                                                color: Colors.grey,
                                                size: 32,
                                            )
                                        ],
                                    ),
                                ],
                            ),
                        ),
                        // Divider(
                        //     height: 2,
                        //     color: Colors.grey,
                        // )
                    ],
                ),
            ),
        );
    }
}