import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../models/user.dart';

class ChatScreen extends StatefulWidget {
    final String chatId;
    final User user;
    final Map peddler;

    ChatScreen({
        this.chatId,
        this.user,
        this.peddler
    });

    @override
    _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
    final TextEditingController _textController = new TextEditingController();
    bool _isComposing = false;
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Builder(
                builder: (context) {
                    return SafeArea(
                        child: Column(
                            children: <Widget>[
                                Flexible(
                                    child: CustomScrollView(
                                        reverse: true,
                                            shrinkWrap: true,
                                            slivers: <Widget>[
                                                SliverPadding(
                                                    padding: const EdgeInsets.all(20.0),
                                                    sliver: SliverList(
                                                        delegate: SliverChildListDelegate(
                                                        <Widget>[
                                                            Text(widget.chatId),
                                                            const Text('Domestic life was never quite my style'),
                                                            const Text('When you smile, you knock me out, I fall apart'),
                                                            const Text('And I thought I was so smart'),
                                                        ],),
                                                    ),
                                                
                                                ),
                                            ],
                                    )
                                ),
                                Container(
                                    child: _buildTextComposer(),
                                )
                            ],
                        ),
                    );
                },
            ),
        ); 
    }

    _buildTextComposer() {
        return Row(children: <Widget>[
            Flexible(
                child: CupertinoTextField(
                    controller: _textController,
                    onChanged: (String text) {
                        // setState(() {
                        //     _isComposing = text.length > 0;
                        // });
                    },
                    onSubmitted: _handleSubmitted,
                ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: CupertinoButton(
                    child: Text('Send'),
                    onPressed: () {},
                    // _isComposing ? () => _handleSubmitted(_textController.text) : null
                ),
            )
        ],);
    }

    void _handleSubmitted(String Text) {

    }
}
