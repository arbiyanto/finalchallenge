import 'dart:async';
// import plugins
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import components
import './components/login_button.dart';
import './components/peddler_register_name.dart';
import './components/peddler_register_fullname.dart';
import './components/peddler_register_picture.dart';
import './components/peddler_register_product.dart';
import './components/peddlers.dart';

// import screens
import '../pages/template.dart';

class CreatePeddlerScreen extends StatefulWidget {
    @override
    _CreatePeddlerScreenState createState() => _CreatePeddlerScreenState();
}

class _CreatePeddlerScreenState extends State<CreatePeddlerScreen> {
    int currentPage = 0;
    bool _isNextDisabled = false;
    bool _isLoading = false;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    @override
    void initState() {
        super.initState();

        print("Creating a StreamController...");
        PeddlerService.isNextDisabled.stream.listen((data) {
            print(data);
            setState(() {
                this._isNextDisabled = data;
            });
        }, onDone: () {
            print("Task Done");
        }, onError: (error) {
            print("Some Error");
        });
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: _scaffoldKey,
            backgroundColor: Color.fromRGBO(255, 149, 0, 1),
            body: Builder(
                builder: (BuildContext context) {
                    return SafeArea(
                        child: Stack(
                            children: <Widget>[
                                SingleChildScrollView(
                                    child: Padding(
                                        padding: EdgeInsets.only(left: 20, right: 20),
                                        child: FutureBuilder(
                                            future: isPage(),
                                            builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                                                switch (snapshot.data) {
                                                    case 0:
                                                        return PeddlerNameSection();
                                                    break;
                                                    case 1:
                                                        return PeddlerFullnameSection();
                                                    break;
                                                    case 2:
                                                        return PeddlerPictureSection();
                                                    break;
                                                    case 3:
                                                        return PeddlerProductSection();
                                                    break;
                                                    default:
                                                        return Text('page not found');
                                                    break;
                                                }
                                            },
                                        )
                                    ),
                                ),
                                // Positioned area
                                Stack(
                                    children: <Widget>[
                                        Positioned(
                                            left: 0,
                                            right: 0,
                                            bottom: 0,
                                            child: LoginButton(
                                                text: 'Lanjut',
                                                isDisabled: this._isNextDisabled,
                                                loginSubmit: this._submitNext,
                                                isLoading: this._isLoading,
                                            ),
                                        )
                                    ],
                                )
                            ],
                        ),
                    );
                },
            ),
        );
    }

    Future<int> isPage() async => this.currentPage;

    _submitNext() {
        var submitted;
        switch (this.currentPage) {
            case 0:
                submitted = PeddlerName.submit();
            break;
            case 1:
                submitted = PeddlerFullname.submit();
            break;
            case 2:
                submitted = PeddlerPicture.submit();
            break;
            case 3:
                this._isNextDisabled = true;
                this._isLoading = true;
                submitted = PeddlerProduct.submit();
            break;
            default:
        }

        submitted.then((data) {
            if (data) {
                if (this.currentPage < 3) {
                    setState(() {
                        this.currentPage += 1;
                    });
                } else {
                    Navigator.pushReplacement(context, 
                        CupertinoPageRoute(
                            builder: (context) => Template()
                        )
                    );
                }
            } else {
                setState(() {
                    this._isLoading = false;
                    this._isNextDisabled = false;
                    print('error async');
                });
            }
        });   
    }

    @override
    void dispose() {
        PeddlerService.isNextDisabled.close(); //Streams must be closed when not needed
        super.dispose();
    }
}
