
// import plugins
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import components
import './components/logo_icon.dart';
import './components/login_button.dart';

// import screen to navigate
import './create_peddler_screen.dart';
import '../pages/home_screen.dart';

class VerifyScreen extends StatefulWidget {
    String verificationId;
    String phone;

    VerifyScreen({ 
        @required this.verificationId,
        @required this.phone
    });

    @override
    _VerifyScreenState createState() => _VerifyScreenState(
        verificationId: this.verificationId
    );
}

class _VerifyScreenState extends State<VerifyScreen> {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    List<TextEditingController> _codeControllers = [];
    List<FocusNode> _codeFocusList = [];
    int currentFocus = 0;
    int codeDigits = 6;
    bool _isLoginButtonDisabled = true;
    bool _isLoading = false;

    String verificationId;

    _VerifyScreenState({
        Key key, @required this.verificationId
    });

    @override
    void dispose() {
        // Clean up the focus node when the Form is disposed
        for(var i = 0; i < this.codeDigits; i++) {
            this._codeFocusList[i].dispose();
        }

        super.dispose();
    }
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            backgroundColor: Color.fromRGBO(255, 149, 0, 1),
            body: Builder(
                builder: (BuildContext context) {
                    return SafeArea(
                        child: Stack(
                            children: <Widget>[
                                // For input and logo
                                Center(
                                    heightFactor: 1.1,
                                    child: Logo(),
                                ),
                                // Sticky button
                                Stack(
                                    children: <Widget>[
                                        Positioned(
                                            left: 0.0,
                                            right: 0.0,
                                            bottom: 80,
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    right: 20,
                                                    left: 20
                                                ),
                                                child: Row(
                                                    children: codeInputList()
                                                ),
                                            ),
                                        ),
                                        Positioned(
                                            left: 0.0,
                                            right: 0.0,
                                            bottom: 0.0,
                                            child: LoginButton(
                                                text: 'Masuk',
                                                isDisabled: this._isLoginButtonDisabled,
                                                loginSubmit: this._submitLogin,
                                                isLoading: this._isLoading,
                                            ),
                                        )
                                    ],
                                )
                            ],
                        ),
                    );
                }
            ),
        );
    }

    List<Widget> codeInputList() {
        List<Widget> list = List();

        for (var i = 0; i < this.codeDigits; i++) {
            this._codeControllers.add(TextEditingController());
            this._codeFocusList.add(FocusNode());

            list.add(
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        child: verificationCodeInput(
                            this._codeControllers[i],
                            this._codeFocusList[i],
                            i
                        )
                    )
                )
            );
        }

        return list;
    }

    Widget verificationCodeInput(controller, focus, currentIteration) {
        var autoFocus = (currentIteration == 0) ? true : false;
        return CupertinoTextField(
            controller: controller,
            focusNode: focus,
            autofocus: autoFocus,
            decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.7),
                border: Border(
                    bottom: BorderSide(
                        width: 3,
                        color: Colors.white
                    )
                )
            ),
            style: TextStyle(
                color: Color.fromRGBO(33, 33, 3, 1),
                fontSize: 32
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.go,
            maxLength: 1,
            onChanged: (text) {
                if (text == null || text == '') {
                    // if input changed to empty, then focus back to previous input
                    this._makeFocus(false, currentIteration);
                } else if (this.currentFocus < 6){
                    this._makeFocus(true, currentIteration);
                } 
                
                if (this.currentFocus >= 6) {
                    setState(() {
                        this._isLoginButtonDisabled = false;
                    });
                } else {
                    if (!this._isLoginButtonDisabled) {
                        setState(() {
                            this._isLoginButtonDisabled = true;
                        });
                    }
                }
            },
        );
    }
    
    // Make input focus on right places
    _makeFocus(bool increment, int currentIteration) {
        this.currentFocus = (increment) ? currentIteration + 1 : currentIteration - 1;
        if (this.currentFocus < 6) {
            FocusScope.of(context).requestFocus(
                this._codeFocusList[this.currentFocus]
            );
        }
    }
    
    // Submitted Form
    _submitLogin() {
        // merge the code inputs
        String code = '';
        for (var controller in this._codeControllers) {
            code = code + controller.text; 
        }
        // if code is filled
        if (code.length == 6) {
            setState(() {
                this._isLoading = true;
                this._isLoginButtonDisabled = true;
            });
            _testSignInWithPhoneNumber(code);
        }
    }

    _testSignInWithPhoneNumber(String smsCode) async {
        // Sign in with verification code
        final AuthCredential credential = PhoneAuthProvider.getCredential(
            verificationId: this.verificationId,
            smsCode: smsCode,
        );
        final FirebaseUser user = await _auth.signInWithCredential(credential);
        final FirebaseUser currentUser = await _auth.currentUser();
        assert(user.uid == currentUser.uid);
        SharedPreferences prefs = await SharedPreferences.getInstance();

        if (user != null) {
            // get the peddlers data
            final QuerySnapshot result =
            await Firestore.instance.collection('peddlers').where('id', isEqualTo: user.uid).getDocuments();
            final List<DocumentSnapshot> documents = result.documents;
            // Declare keysets which one to save
            List<String> keySets = [
                'id','name','fullname','kategori','image','description','activeStatus'
            ];

            print(documents.length);
            print(user.uid);

            if (documents.length == 0) {
                // Update data to server if new user
                final userData = {
                    'id': user.uid,
                    'name': '',
                    'fullname': '',
                    'kategori': '',
                    'image': '',
                    'description': '',
                    'activeStatus': true,
                    'phone':widget.phone
                };
                await Firestore.instance.collection('peddlers').document(user.uid).setData(userData);

                for (String item in keySets) {
                    // check data types of document
                    if (userData[item] is String) await prefs.setString(item, userData[item]);
                    if (userData[item] is int) await prefs.setInt(item, userData[item]);
                    if (userData[item] is bool) await prefs.setBool(item, userData[item]);
                }

                Navigator.push(context, CupertinoPageRoute(
                    builder: (context) => CreatePeddlerScreen()
                ));
            } else {
                // Write data to local
                // Loop the keysets
                for (String item in keySets) {
                    // check data types of document
                    if (documents[0][item] is String) await prefs.setString(item, documents[0][item]);
                    if (documents[0][item] is int) await prefs.setInt(item, documents[0][item]);
                    if (documents[0][item] is bool) await prefs.setBool(item, documents[0][item]);
                }

                if (documents[0]['name'] != '') {
                    Navigator.push(context, CupertinoPageRoute(
                        builder: (context) => HomeScreen()
                    ));
                } else {
                    Navigator.push(context, CupertinoPageRoute(
                        builder: (context) => CreatePeddlerScreen()
                    ));
                }
            }
        }
    }
}