import 'dart:async';
// import plugins
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

// import components
import './components/logo_icon.dart';
import './components/login_button.dart';

// import screen
import './verify_screen.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    // Immutable properties
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final String testSmsCode = '123456';

    // Mutable properties
    TextEditingController _phoneInputController = TextEditingController();
    String verificationId;
    bool _isLoginButtonDisabled = true;
    bool _isLoading = false;
    String phone;

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        // _phoneInputController.text = '4085556969';
        return Scaffold(
            backgroundColor: Color.fromRGBO(255, 149, 0, 1),
            body: Builder(
                builder: (BuildContext context) {
                    return SafeArea(
                        child: Stack(
                            children: <Widget>[
                                Center(
                                    heightFactor: 1.1,
                                    child: Logo()
                                ),
                                Stack(
                                    children: <Widget>[
                                        Positioned(
                                            left: 0,
                                            right: 0,
                                            bottom: 80,
                                            child: Padding(
                                                padding: EdgeInsets.all(20),
                                                child: Row(
                                                    children: <Widget>[
                                                        Padding(
                                                            padding: EdgeInsets.only(right: 5),
                                                            child: Text(
                                                                '+62',
                                                                style: TextStyle(
                                                                    color: Color.fromRGBO(33, 33, 3, 1),
                                                                    fontSize: 32
                                                                ),
                                                            ),
                                                        ),
                                                        Expanded(child: phoneInput(),)
                                                    ],
                                                ),
                                            ),
                                        ),
                                        Positioned(
                                            left: 0.0,
                                            right: 0.0,
                                            bottom: 0,
                                            child: LoginButton(
                                                text: 'Kirim Kode',
                                                isDisabled: this._isLoginButtonDisabled,
                                                loginSubmit: this._loginSubmit,
                                                isLoading: this._isLoading
                                            )
                                        )
                                    ],
                                )
                            ],
                        ),
                    );
                },
            )
        );
    }

    Widget phoneInput() {
        return CupertinoTextField(
            controller: _phoneInputController,
            decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.7),
                borderRadius: BorderRadius.all(
                    Radius.circular(2)
                ),
            ),
            style: TextStyle(
                color: Color.fromRGBO(33, 33, 3, 1),
                fontSize: 32
            ),
            keyboardType: TextInputType.phone,
            textInputAction: TextInputAction.go,
            placeholder: 'Nomor Telepon',
            autofocus: true,
            onChanged: (text) {
                setState(() {
                    this._isLoginButtonDisabled = text == '' || text == null ? true : false;
                });
            },
        );
    }


    Future<void> _testVerifyPhoneNumber(String phoneNumber) async {
        final PhoneVerificationCompleted verificationCompleted =
            (FirebaseUser user) {
        };

        final PhoneVerificationFailed verificationFailed = (AuthException authException) {
            setState(() {
                this._isLoading = false;
                this._isLoginButtonDisabled = false;
            });
        };

        final PhoneCodeSent codeSent =
            (String verificationId, [int forceResendingToken]) async {
            this.verificationId = verificationId;
      
            Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (context) => VerifyScreen(
                        verificationId: verificationId,
                        phone: this.phone,
                    )
                )
            );
        };

        final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
            (String verificationId) {
            this.verificationId = verificationId;
        };

        await _auth.verifyPhoneNumber(
            phoneNumber: phoneNumber,
            timeout: const Duration(seconds: 5),
            verificationCompleted: verificationCompleted,
            verificationFailed: verificationFailed,
            codeSent: codeSent,
            codeAutoRetrievalTimeout: codeAutoRetrievalTimeout
        );
    }

    _loginSubmit() {
        this.phone = '+62' +  _phoneInputController.text;
        setState(() {
            this._isLoginButtonDisabled = true;
            this._isLoading = true;    
        });
        
        _testVerifyPhoneNumber(phone);
    }
}

// FutureBuilder<String>(
//     future: _message,
//     builder: (_, AsyncSnapshot<String> snapshot) {
//         return Text(snapshot.data ?? '',
//             style: const TextStyle(color: Color.fromARGB(255, 0, 155, 0))
//         );
//     }
// ),