import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:meta/meta.dart';

Widget LoginButton({
    @required String text,
    @required bool isDisabled, 
    @required VoidCallback loginSubmit,
    bool isLoading = false
}) {
    return Container(
        height: 60,
        child: SizedBox.expand(
            child: CupertinoButton(
                child: (!isLoading) ? Text(
                    text,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 22,
                        letterSpacing: 0.35
                    ),
                ) : SpinKitThreeBounce(
                    color: Colors.white,
                    size: 35.0,
                ),
                color: Color.fromRGBO(26, 104, 223, 1),
                borderRadius: BorderRadius.all(
                    Radius.zero
                ),
                onPressed: isDisabled ? null : () {
                    loginSubmit();
                },
            )
        ),
    );
}