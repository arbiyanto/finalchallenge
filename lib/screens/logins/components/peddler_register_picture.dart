import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:uuid/uuid.dart';

import './peddlers.dart';

String pictureName = '';

class PeddlerPictureSection extends StatefulWidget {
    @override
    _PeddlerPictureSectionState createState() => _PeddlerPictureSectionState();
}

class _PeddlerPictureSectionState extends State<PeddlerPictureSection> {
    FirebaseStorage storage;
    File picture;
    bool isUploading = false;

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.only(top: 65),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    PeddlerTitle('Tambahkan Foto'),
                    PeddlerDescription('Foto ini akan ditampilkan ke pembeli. Anda dapat mengunggah foto gerobak Anda.'),
                    Center(
                        child: GestureDetector(
                            onTap: () {
                                getImage();
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(29)
                                    ),
                                    color: Color.fromRGBO(216, 216, 216, 0.54),
                                    image: picture == null ? null : DecorationImage(
                                        fit: BoxFit.cover,
                                        alignment: FractionalOffset.topCenter,
                                        image: Image.file(picture).image
                                    )
                                ),
                                height: 265,
                                width: 265,
                                child: picture == null ? CameraIcon() :  LoadingUpload() ,
                            ),
                        ),
                    )
                    
                ],
            ),
        );
    }

    Future<bool> uploadingBuilder() async => this.isUploading;

    Widget CameraIcon() {
        return Icon(
            Icons.photo_camera,
            size: 50,
            color: Color.fromRGBO(170, 41, 23, 1.0),
        );
    }

    Widget LoadingUpload() {
        if (this.isUploading) {
            return Center(
                child: SpinKitCircle(
                    size: 50,
                    color: Colors.white,
                )
            );
        } else {
            return Container();
        }
    }

    Future getImage() async {
        var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 270, maxHeight: 270);

        if (tempImage == null ) {
            print('canceled');
        } else {
            final String uuid = Uuid().v1();
            final String filename = uuid+'.png';

            setState(() {
                this.isUploading = true;
                picture = tempImage;
            });

            PeddlerService.isNextDisabled.add(true);

            final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(filename);
            final StorageUploadTask task = firebaseStorageRef.putFile(picture);

            task.onComplete.then((data) {
                if(task.isSuccessful) {
                    // set to global
                    pictureName = filename;
                    PeddlerService.isNextDisabled.add(false);
                    setState(() {
                        this.isUploading = false;
                    });
                    print('success uploading');
                }

                if (task.isCanceled) {
                    print('upload canceled');
                }
            });
        }
        
    }
}

class PeddlerPicture {

    static Future<bool> submit() async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('image', pictureName);
        return Future.value(true);
    }

}