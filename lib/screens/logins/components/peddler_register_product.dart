import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import './peddlers.dart';

class PeddlerProductSection extends StatefulWidget {
    @override
    _PeddlerProductSectionState createState() => _PeddlerProductSectionState();
}

List<Map> data = [
    { 
        'picture' : '', 
        'price': MoneyMaskedTextController(
            decimalSeparator: '', 
            precision: 0,
            thousandSeparator: '.',
            leftSymbol: 'Rp '
        ), 
        'name': TextEditingController(text: '') 
    }
];

class _PeddlerProductSectionState extends State<PeddlerProductSection> {
    
    List<File> picture = [null];
    List isUploading = [false];
    
    @override
    Widget build(BuildContext context) {
        int i = -1;
        return SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(top: 65,bottom: 85),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        PeddlerTitle('Tambahkan Produk yang Dijual'),
                        PeddlerDescription('Silahkan tambahkan foto dan nama produk Anda jika diperlukan.'),
                        Column(
                            children: data.map<Widget>((data) => listProduct(data, i+=1)).toList(),
                        ),
                        Center(
                            child: OutlineButton(
                                child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                        Icon(Icons.add, color: Colors.white,size: 16,),
                                        Text('Tambah Menu Lagi', style: TextStyle(
                                            color: Colors.white
                                        ),),
                                    ],
                                ),
                                color: Color.fromRGBO(26, 104, 223, 0),
                                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                                onPressed: () {
                                    setState(() {
                                        this.isUploading.add(false);
                                        this.picture.add(null);
                                        data.add({ 'picture' : '', 'price': MoneyMaskedTextController(precision: 0, thousandSeparator: '.', decimalSeparator: '', leftSymbol: 'Rp '), 'name': TextEditingController(text: ''),  });
                                    });
                                },
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    Widget listProduct(product, index) {
        return Container(
            padding: EdgeInsets.only(bottom: 15),
            child: Row(
                children: <Widget>[
                    GestureDetector(
                        onTap: () {
                            getImage(product, index);
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5)
                                ),
                                color: Color.fromRGBO(216, 216, 216, 0.54),
                                image: picture[index] == null ? null : DecorationImage(
                                    fit: BoxFit.cover,
                                    alignment: FractionalOffset.topCenter,
                                    image: Image.file(picture[index]).image
                                )
                            ),
                            height: 100,
                            width: 100,
                            child: picture[index] == null ? CameraIcon() :  LoadingUpload(index) ,
                        ),
                    ),
                    Expanded(
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Column(
                                children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: CupertinoTextField(
                                            controller: product['name'],
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(255, 255, 255, 0.7),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(2)
                                                ),
                                            ),
                                            style: TextStyle(
                                                color: Color.fromRGBO(33, 33, 3, 1),
                                                fontSize: 16
                                            ),
                                            placeholder: 'Nama Menu',
                                            keyboardType: TextInputType.text,
                                        ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: CupertinoTextField(
                                            controller: product['price'],
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(255, 255, 255, 0.7),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(2)
                                                ),
                                            ),
                                            style: TextStyle(
                                                color: Color.fromRGBO(33, 33, 3, 1),
                                                fontSize: 16
                                            ),
                                            placeholder: 'Harga. Contoh: 15000',
                                            keyboardType: TextInputType.number,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    )
                ],
            ),
        );
    }

    Widget CameraIcon() {
        return Icon(
            Icons.photo_camera,
            size: 20,
            color: Color.fromRGBO(170, 41, 23, 1.0),
        );
    }

    Widget LoadingUpload(index) {
        if (this.isUploading[index]) {
            return Center(
                child: SpinKitCircle(
                    size: 20,
                    color: Colors.white,
                )
            );
        } else {
            return Container();
        }
    }

    Future getImage(product, index) async {
        var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 270, maxHeight: 270);

        if (tempImage == null ) {
            print('canceled');
        } else {
            final String uuid = Uuid().v1();
            final String filename = uuid+'.png';

            setState(() {
                this.isUploading[index] = true;
                this.picture[index] = tempImage;
            });

            PeddlerService.isNextDisabled.add(true);

            final StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(filename);
            final StorageUploadTask task = firebaseStorageRef.putFile(picture[index]);

            task.onComplete.then((d) {
                if(task.isSuccessful) {
                    // set to global
                    product['picture'] = filename;
                    PeddlerService.isNextDisabled.add(false);
                    setState(() {
                        this.isUploading[index] = false;
                    });
                    print('success uploading');
                }

                if (task.isCanceled) {
                    print('upload canceled');
                }
            });
        }
        
    }
}

class PeddlerProduct {
    static Future<bool> submit() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final user_id = prefs.getString('id');
        final DocumentReference postRef = Firestore.instance.document('peddlers/$user_id');
        PeddlerService.isNextDisabled.add(true);

        await Firestore.instance.runTransaction((Transaction tx) async {
            DocumentSnapshot postSnapshot = await tx.get(postRef);
            if (postSnapshot.exists) {
                await tx.update(postRef, <String, dynamic>{
                    'name': prefs.getString('name'),
                    'fullname': prefs.getString('fullname'),
                    'image': prefs.getString('image'),
                });

                data.forEach((d) async{
                    if (d['name'].text != null || d['name'].text != '') {
                        String generatedId = Uuid().v4();
                        await Firestore.instance.document('peddlers/$user_id').collection('menu').document(generatedId).setData({
                            'menuPic': d['picture'],
                            'harga': (d['price'] != null) ? d['price'].text : '',
                            'name': d['name'].text,
                            'description': ''
                        });
                    }
                });
                
            }
        }).catchError((error) {
            print('$error');
            return Future.value(false);
        });

        // PeddlerService.isNextDisabled.add(false);
        
        return Future.value(true);
    }
}