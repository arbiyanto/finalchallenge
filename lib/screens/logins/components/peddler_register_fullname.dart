import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './peddlers.dart';

TextEditingController controller = TextEditingController();

class PeddlerFullnameSection extends StatefulWidget {
    @override
    _PeddlerFullnameSectionState createState() => _PeddlerFullnameSectionState();
}

class _PeddlerFullnameSectionState extends State<PeddlerFullnameSection> {
    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.only(top: 65),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    PeddlerTitle('Nama Usaha'),
                    PeddlerDescription('Nama usaha Anda yang akan ditampilkan ke pembeli.'),
                    CupertinoTextField(
                        controller: controller,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 0.7),
                            borderRadius: BorderRadius.all(
                                Radius.circular(2)
                            ),
                        ),
                        style: TextStyle(
                            color: Color.fromRGBO(33, 33, 3, 1),
                            fontSize: 32
                        ),
                        placeholder: 'Nama Usaha',
                        autofocus: true,
                        keyboardType: TextInputType.text,
                    ),
                ],
            ),
        );
    }

}

class PeddlerFullname {
    static Future<bool> submit() async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('name', controller.text);
        return Future.value(true);
    }
}