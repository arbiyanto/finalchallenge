import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

Widget PeddlerTitle(String text) {
    return Text(
        text,
        style: const TextStyle(
            color: Colors.white,
            fontSize: 36,
            letterSpacing: 0.41,
            fontWeight: FontWeight.w600,
        ),
    );
}

Widget PeddlerDescription(String text) {
    return Padding(
        padding: EdgeInsets.only(top: 20, bottom: 20),
        child: Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 27,
                fontWeight: FontWeight.w400,
                letterSpacing: 0.46
            ),
        )
    );
}

class PeddlerService {
    static StreamController<bool> isNextDisabled = new StreamController();
}