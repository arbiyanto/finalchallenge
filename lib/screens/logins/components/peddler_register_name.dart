import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './peddlers.dart';

TextEditingController controller = TextEditingController();

class PeddlerNameSection extends StatefulWidget {
    @override
    _PeddlerNameSectionState createState() => _PeddlerNameSectionState();
}

class _PeddlerNameSectionState extends State<PeddlerNameSection> {
    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.only(top: 65),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    PeddlerTitle('Selamat Datang!'),
                    PeddlerDescription('Silahkan isi nama lengkap Anda.'),
                    CupertinoTextField(
                        controller: controller,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 0.7),
                            borderRadius: BorderRadius.all(
                                Radius.circular(2)
                            ),
                        ),
                        style: TextStyle(
                            color: Color.fromRGBO(33, 33, 3, 1),
                            fontSize: 32
                        ),
                        placeholder: 'Nama Anda',
                        autofocus: true,
                        keyboardType: TextInputType.text,
                    )
                ],
            ),
        );
    }
}

class PeddlerName {
    PeddlerName({Key key});

    static Future<bool> submit() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('fullname', controller.text);
        return Future.value(true);
    }
}
