import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

enum AuthStatus {
    authenticated,
    needLogin,
    needRegister,
}

class Auth {
    static List keys = [
        'id',
        'name',
        'fullname',
        'category',
        'image',
        'description',
        'activeStatus',
    ];

    static const login = null;

    static Future<AuthStatus> isAuthenticated() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final name = prefs.getString('name');
        final id = prefs.getString('id');
        
        if (id == null || id == '') {
            return Future.value(AuthStatus.needLogin);
        } else if (name == null || name == '') {
            return Future.value(AuthStatus.needRegister);
        } else {
            return Future.value(AuthStatus.authenticated);
        }
    }

    static clear() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.clear();
    }

    static Future<Map> getLocalUser() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();

        final user = Map();

        for (var item in keys) {
            user[item] = prefs.get(item);
        }

        return Future.value(user);
    }
    
}