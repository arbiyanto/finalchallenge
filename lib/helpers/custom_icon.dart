import 'package:flutter/cupertino.dart';

class CustomIcon extends StatelessWidget {
    const CustomIcon(this.image);
    final String image;

    @override
    Widget build(BuildContext context) {
        return Image(
            image: AssetImage(this.image),
            fit: BoxFit.scaleDown,
            alignment: Alignment.center,
            width: 35,
            height: 35,
        );
    }
}