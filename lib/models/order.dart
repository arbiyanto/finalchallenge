import 'package:cloud_firestore/cloud_firestore.dart';

class Order {
    final String orderID;
    final String orderStatus;
    final String picture;
    final String userID;
    final String userName;
    final Timestamp created_at;
    final DocumentReference reference;

    // Initialize the property from map data
    Order.fromMap(Map<String, dynamic> map, {this.reference}):
        // Make sure the variable has a non-null value. 
        assert(map['orderID'] != null),
        assert(map['orderStatus'] != null),
        assert(map['picture'] != null),
        assert(map['userID'] != null),
        assert(map['userName'] != null),
        assert(map['created_at'] != null),
        // define variable to property
        orderID = map['orderID'],
        orderStatus = map['orderStatus'],
        picture = map['picture'],
        userName = map['userName'],
        userID = map['userID'],
        created_at = map['created_at'];

    
    // Initialize the property from snapshot
    Order.fromSnapshot(DocumentSnapshot snapshot)
        : this.fromMap(snapshot.data, reference: snapshot.reference);
}

class OrderUser {
    final String id;
    final String nickname;
    final GeoPoint location;
    final String photoUrl;
    final String phone;

    OrderUser.fromMap(Map<String, dynamic> map): 
        assert(map['id'] != null),
        assert(map['nickname'] != null),
        assert(map['location'] != null),
        assert(map['photoUrl'] != null),
        assert(map['phone'] != null),
        id = map['id'],
        nickname = map['nickname'],
        location = map['location'],
        photoUrl = map['photoUrl'],
        phone = map['phone'];
}