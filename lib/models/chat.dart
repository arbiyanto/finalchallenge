import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class Chat {
    final String id;
    final String userId;
    final String peddlerId;
    final String lastMessageBody;
    final bool lastMessageRead;
    final String lastMessageSender;
    final Timestamp lastMessageSent;
    final DocumentReference reference;

    // Initialize the property from map data
    Chat.fromMap(Map<String, dynamic> map, {this.reference}):
        // Make sure the variable has a non-null value. 
        assert(map['id'] != null),
        assert(map['userId'] != null),
        assert(map['peddlerId'] != null),
        assert(map['lastMessageBody'] != null),
        assert(map['lastMessageRead'] != null),
        assert(map['lastMessageSender'] != null),
        assert(map['lastMessageSent'] != null),
        // define variable to property
        id = map['id'],
        userId = map['userId'],
        peddlerId = map['peddlerId'],
        lastMessageBody = map['lastMessageBody'],
        lastMessageRead = map['lastMessageRead'],
        lastMessageSender = map['lastMessageSender'],
        lastMessageSent = map['lastMessageSent'];
    
    // Initialize the property from snapshot
    Chat.fromSnapshot(DocumentSnapshot snapshot)
        : this.fromMap(snapshot.data, reference: snapshot.reference);

    // Get chat list with pagination
    static Stream<QuerySnapshot> getChats({
        @required String peddler_id,
        @required String last_document_id
    }) {
        var instance = Firestore.instance
            .collection('chats')
            .where('peddlerId', isEqualTo: peddler_id)
            // .orderBy('lastMessageSent') // still bug issue: https://github.com/flutter/flutter/issues/15928
            .limit(8)
            ;
        
        if (last_document_id != null) {
            instance = instance.startAfter([{'id': last_document_id}]);
        }

        return instance.snapshots();
    }

}