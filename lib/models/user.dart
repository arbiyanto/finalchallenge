import 'package:cloud_firestore/cloud_firestore.dart';

class User {
    final String id;
    final String email;
    final String nickname;
    final String phone;
    final String photoUrl;
    final GeoPoint location;
    final locLastUpdated;
    final DocumentReference reference;

    // Initialize the property from map data
    User.fromMap(Map<String, dynamic> map, {this.reference}):
        // Make sure the variable has a non-null value. 
        assert(map['id'] != null),
        assert(map['email'] != null),
        assert(map['nickname'] != null),
        assert(map['phone'] != null),
        assert(map['photoUrl'] != null),
        assert(map['location'] != null),
        assert(map['locLastUpdated'] != null),
        // define variable to property
        id = map['id'],
        email = map['email'],
        nickname = map['nickname'],
        phone = map['phone'],
        photoUrl = map['photoUrl'],
        location = map['location'],
        locLastUpdated = map['locLastUpdated'];
    
    // Initialize the property from snapshot
    User.fromSnapshot(DocumentSnapshot snapshot)
        : this.fromMap(snapshot.data, reference: snapshot.reference);


}