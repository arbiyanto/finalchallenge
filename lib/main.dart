// import dependencies
import 'package:flutter/cupertino.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// import helpers
import './helpers/auth.dart';

import './screens/logins/login_screen.dart';
import './screens/pages/template.dart';
import './screens/logins/create_peddler_screen.dart';

void main() => runApp(MyApp());

// Solve the issue that raise because localization. because of that we cannot show the dialog using cupertino.
// The getter 'alertDialogLabel' was called on null.
// Issue: https://github.com/flutter/flutter/issues/23047
class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
    const FallbackCupertinoLocalisationsDelegate();

    @override
    bool isSupported(Locale locale) => true;

    @override
    Future<CupertinoLocalizations> load(Locale locale) =>
    DefaultCupertinoLocalizations.load(locale);

    @override
    bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}

class MyApp extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        Auth.clear();
        return CupertinoApp(
            title: "Golek'i",
            home: FutureBuilder(
                future: Auth.isAuthenticated(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                        return Container();
                    }
                    if (snapshot.data == AuthStatus.authenticated) {
                        return Template();
                        // return CreatePeddlerScreen();
                    } else if (snapshot.data == AuthStatus.needRegister) {
                        return CreatePeddlerScreen();
                    } else {
                        return LoginScreen();
                    }
                },
            ),
            debugShowCheckedModeBanner: false,
            localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                const FallbackCupertinoLocalisationsDelegate(),
            ],
            theme: CupertinoThemeData(
                primaryColor: Color.fromRGBO(255, 149, 0, 1.0),
                
            ),
            supportedLocales: [
                const Locale('id'), // English
                // ... other locales the app supports
            ],
        );
    }
}

